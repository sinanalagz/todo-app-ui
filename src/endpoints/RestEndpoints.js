const HOST = "http://46.101.23.176";
const PORT = "3001";

export const BASE_URL = `${HOST}:${PORT}`;
export const saveTodoEndpoint = `/save`;
export const getTodosEndpoint = `/findAll`;
export const updateTodoEndpoint = `/update`;

