import {useDispatch, useSelector} from "react-redux";
import TodoList from "../../components/TodoList/TodoList";
import {useEffect} from "react";
import {todoService} from "../../fetch/services";
import {setTodosActions} from "../../state/actions";

export function TodoListContainer() {
    const dispatch = useDispatch();
    const todoListState = useSelector(state => state.todoList);
    const {todos} = todoListState;

    useEffect(() => {
        async function fetchGetTodos() {
            const todoList = await todoService.getTodos();
            dispatch(setTodosActions(todoList));
        }

        fetchGetTodos();
    });

    return (
        <TodoList todos={todos}/>
    );
}