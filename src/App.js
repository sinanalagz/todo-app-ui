import './App.css';
import Header from "./components/Header/Header";
import AddInput from "./components/AddInput/AddInput";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {TodoListContainer} from "./containers/TodoListContainer/TodoListContainer";

function App() {
    return (
        <div className="App">
            <div className="app-container">
                <ToastContainer/>
                <Header title="Todo List"/>
                <AddInput/>
                <TodoListContainer/>
            </div>
        </div>
    );
}

export default App;
