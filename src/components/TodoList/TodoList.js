import './TodoList.css'
import {todoService} from "../../fetch/services";
import {toast} from "react-toastify";
import {useDispatch} from "react-redux";
import {setTodosActions} from "../../state/actions";

function TodoList({todos}) {
    const dispatch = useDispatch();

    const updateTodo = async (id, status) => {
        try {
            await todoService.updateTodo(id, status)
            toast.success("Todo updated", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
            });
            const todos = await todoService.getTodos();
            dispatch(setTodosActions(todos));
        } catch (e) {
            toast.error("Unexpected Error", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
            });
        }
    };

    return (
        <ul className="todo-list">
            {
                todos && todos.map(todo => (
                    <li
                        key={todo.id}
                        className={`todo-item- ${todo.completed && "todo-item-completed"}`}
                        onClick={() => updateTodo(todo.id, !todo.completed)}
                    >
                        {todo.task}
                    </li>
                ))
            }
        </ul>
    );
}

export default TodoList;