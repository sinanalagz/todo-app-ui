import {fireEvent, render, screen} from '@testing-library/react';
import React from "react";
import '@testing-library/jest-dom'
import TodoList from "../TodoList";
import AddInput from "../../AddInput/AddInput";
import {Provider} from "react-redux";
import {store} from "../../../state";
import {setupServer} from "msw/node";
import {rest} from "msw";
import {getTodosEndpoint, saveTodoEndpoint, updateTodoEndpoint} from "../../../endpoints/RestEndpoints";
import Todo from "../../../fetch/todo";

const server = setupServer(
    rest.post(updateTodoEndpoint, (req, res, ctx) => {
        return res(ctx.body(new Todo("Todo 1", true, 1)))
    }),

    rest.get(getTodosEndpoint, (req, res, ctx) => {
        return res(ctx.json([{
            id: 1,
            task: 'Todo 1',
            completed: false
        }]))
    }),
);

const MockTodoList = () => {
    return (
        <Provider store={store}>
            <TodoList todos={[{id: 1, task: 'Todo 1', complated: false}]}/>
        </Provider>
    )
};

describe("AddInput", () => {

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    it('should have list item', async () => {
        render(
            <MockTodoList/>
        );
        const todoItem = screen.getByText('Todo 1');
        expect(todoItem).toBeInTheDocument();
    });

    /*it('completed todo', () => {
        render(
            <MockTodoList/>
        );
        const todoItem = screen.getByText('Todo 1');
        fireEvent.click(todoItem);
        expect(todoItem).toHaveStyle('color: green')
        /!*expect(todoItem).toHaveStyle({
            color: 'green',
        })*!/
    })*/
});