import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import AddInput from "../AddInput"
import {store} from "../../../state";
import {Provider} from "react-redux";
import React from "react";
import {rest} from 'msw'
import {setupServer} from 'msw/node'
import '@testing-library/jest-dom'
import {getTodosEndpoint, saveTodoEndpoint} from "../../../endpoints/RestEndpoints";
import Todo from "../../../fetch/todo";

const server = setupServer(
    rest.post(saveTodoEndpoint, (req, res, ctx) => {
        return res(ctx.body(new Todo("Todo 1", false, 1)))
    }),

    rest.get(getTodosEndpoint, (req, res, ctx) => {
        return res(ctx.json([{
            id: 1,
            task: 'My Todo 1',
            completed: false
        }]))
    }),
);

const MockAddInput = () => {
    return (
        <Provider store={store}>
            <AddInput/>
        </Provider>
    )
};

describe("AddInput", () => {

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    it('should render input element', () => {
        render(
            <MockAddInput/>
        );
        const inputElement = screen.getByPlaceholderText(/Add a new task here.../i);
        expect(inputElement).toBeInTheDocument();
    });

    it('should render add button', () => {
        render(
            <MockAddInput/>
        );
        const inputElement = screen.getByPlaceholderText(/Add/i);
        expect(inputElement).toBeInTheDocument();
    });

    it('should be change input', () => {
        render(
            <MockAddInput/>
        );
        const inputElement = screen.getByPlaceholderText(/Add a new task here.../i);
        fireEvent.click(inputElement);
        fireEvent.change(inputElement, {target: {value: "Go Grocery Shopping"}});
        expect(inputElement.value).toBe("Go Grocery Shopping");
    });

    it('should have empty input when add button is clicked', async () => {
        render(
            <MockAddInput/>
        );
        const inputElement = screen.getByTestId('new-todo-input');
        fireEvent.change(inputElement, {target: {value: "Go Grocery Shopping"}});
        const buttonElement = screen.getByRole("button", {name: /Add/i});
        fireEvent.click(buttonElement);

        await waitFor(() => screen.getByTestId('new-todo-input'));
        expect(screen.getByTestId('new-todo-input')).toHaveTextContent('')
    });
});