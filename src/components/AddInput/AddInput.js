import {useDispatch, useSelector} from "react-redux";
import {toast} from 'react-toastify';
import {todoService} from "../../fetch/services";
import './AddInput.css'
import {setNewTodoAction, setTodosActions, setWaitingAction} from "../../state/actions";

function AddInput() {
    const dispatch = useDispatch();
    const todoListState = useSelector(state => state.todoList);
    const {newTodo} = todoListState;

    const saveTodo = async () => {
        try {
            dispatch(setWaitingAction(true));
            await todoService.saveTodo({
                task: newTodo,
                completed: false
            });
            toast.success('Todo created', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
            });
            const todoList = await todoService.getTodos();
            dispatch(setTodosActions(todoList));
        } catch (e) {
            console.log(e);
            toast.error('Todo could not be registered', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } finally {
            dispatch(setWaitingAction(false));
            dispatch(setNewTodoAction(''));
        }
    };

    return (
        <div className="add-input-container">
            <div className="input-container">
                <input
                    type="text"
                    className="new-todo-input"
                    placeholder="Add a new task here..."
                    value={newTodo}
                    onChange={e => dispatch(setNewTodoAction(e.target.value))}
                    autoFocus
                    data-testid="new-todo-input"
                />
            </div>

            <div className="btn-container">
                <div>
                    <button
                        className="add-btn"
                        onClick={saveTodo}
                    >
                        Add
                    </button>
                </div>
            </div>
        </div>
    );
}

export default AddInput;