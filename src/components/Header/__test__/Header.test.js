import {render, screen} from '@testing-library/react';
import Header from "../Header";

describe("Header", () => {
    it('should render same text passed into title prop', () => {
        render(
            <Header
                title="my-app"
            />
        );
        const h1Element = screen.getByText(/my-app/i);
        expect(h1Element).toBeInTheDocument();
    });

    it('should render same text passed into title prop as h1', () => {
        render(
            <Header
                title="my-app"
            />
        );
        const h1Element = screen.getByRole("heading");
        expect(h1Element).toBeInTheDocument();
    });
});