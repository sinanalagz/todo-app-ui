class Todo {
    constructor(task, completed, id) {
        this.id = id;
        this.task = task;
        this.completed = completed;
    }
}

export default Todo;