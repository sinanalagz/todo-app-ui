import * as Pact from '@pact-foundation/pact';
import TodoService from "./todo.service";
import Todo from "./todo";
import {getTodosEndpoint, saveTodoEndpoint} from "../endpoints/RestEndpoints";
import {somethingLike} from "@pact-foundation/pact/src/dsl/matchers";

describe('TodoService API', () => {

    const todoService = new TodoService();

    const contentTypeJsonMatcher = Pact.Matchers.term({
        matcher: "application\\/json; *charset=utf-8",
        generate: "application/json; charset=utf-8"
    });

    describe('saveTodo()', () => {

        beforeEach((done) => {
            global.provider.addInteraction({
                state: 'provider allows todo creation',
                uponReceiving: 'a POST request to create a todo',
                withRequest: {
                    method: 'POST',
                    path: saveTodoEndpoint,
                    headers: {
                        'Accept': contentTypeJsonMatcher,
                        'Content-Type': contentTypeJsonMatcher
                    },
                    body: new Todo('Todo 1', false)
                },
                willRespondWith: {
                    status: 201,
                    headers: {
                        'Content-Type': Pact.Matchers.term({
                            matcher: "application\\/json; *charset=utf-8",
                            generate: "application/json; charset=utf-8"
                        })
                    },
                    body: Pact.Matchers.somethingLike(
                        new Todo('Todo 1', false, 1)
                    )
                }
            }).then(() => done());
        });

        it('sends a request according to contract', (done) => {
            todoService.saveTodo(new Todo('Todo 1', false))
                .then(todo => {
                    expect(todo.id).toEqual(1);
                })
                .then(() => {
                    global.provider.verify()
                        .then(() => done(), error => {
                            done.fail(error)
                        })
                });
        });
    });


    describe('getTodos()', () => {

        beforeEach((done) => {
            global.provider.addInteraction({
                state: 'provider allows get todo list',
                uponReceiving: 'a GET request to get todos',
                withRequest: {
                    method: 'GET',
                    path: getTodosEndpoint,
                },
                willRespondWith: {
                    status: 200,
                    body: somethingLike(
                        [new Todo('Todo', false, 1)]
                    ),
                },
            }).then(() => done());
        });

        it('sends a request according to contract', (done) => {
            todoService.getTodos()
                .then(response => {
                    expect(response[0].id).toBe(1);
                })
                .then(() => {
                    global.provider.verify()
                        .then(() => done(), error => {
                            done.fail(error)
                        })
                });
        });
    });

});