import {BASE_URL, getTodosEndpoint, saveTodoEndpoint, updateTodoEndpoint} from "../endpoints/RestEndpoints";
import axios from 'axios';

class TodoService {

    saveTodo(todo) {
        return axios.request({
            method: 'POST',
            url: saveTodoEndpoint,
            baseURL: BASE_URL,
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            },
            data: todo
        }).then((response) => {
            const todo = response.data;
            return new Promise((resolve, reject) => {
                try {
                    resolve(todo);
                } catch (error) {
                    reject(error);
                }
            });
        });
    }

    getTodos = async () => {
        const response = await axios.get(`${BASE_URL}${getTodosEndpoint}`);
        return response.data;
    };

    updateTodo = async (id, status) => {
        await axios.put(`${BASE_URL}${updateTodoEndpoint}`, {id, completed: status});
    }
}

export default TodoService;