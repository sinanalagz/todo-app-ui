export const setWaitingAction = (waitingStatus) => {
    return {
        type: "SET_WAITING",
        payload: waitingStatus
    }
};

export const setTodosActions = (todos) => {
    return {
        type: "SET_TODOS",
        payload: todos
    }
};

export const setNewTodoAction = (newTodo) => {
    return {
        type: "SET_NEW_TODO",
        payload: newTodo
    }
};