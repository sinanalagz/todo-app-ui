const initialState = {
    waiting: false,
    newTodo: "",
    todos: []
};

const reducer = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case "SET_NEW_TODO":
            return {
                ...state,
                newTodo: action.payload,
            };
        case "SET_WAITING":
            return {
                ...state,
                waiting: action.payload,
            };
        case "SET_TODOS":
            return {
                ...state,
                todos: action.payload,
            };
        default:
            return state
    }
};

export default reducer;