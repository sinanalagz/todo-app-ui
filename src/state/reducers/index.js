import { combineReducers } from "redux";
import todoListReducer from "./todoListReducer"

const reducers = combineReducers({
    todoList: todoListReducer
});

export default reducers
