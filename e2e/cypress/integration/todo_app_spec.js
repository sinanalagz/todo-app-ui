import {BASE_URL, getTodosEndpoint, saveTodoEndpoint} from "../../../src/endpoints/RestEndpoints";

describe('example to-do app', () => {
    beforeEach(() => {
        cy.visit('http://159.65.204.255:3001')
    });

    it('display header', () => {
        cy.contains("Todo List")
    });

    it('display add todo input', () => {
        cy.get(".new-todo-input").should("be.visible")
    });

    it('focuses input on load', () => {
        cy.focused().should("have.class", "new-todo-input")
    });

    it('display add button', () => {
        cy.get(".add-btn").should("be.visible")
    });

    it('changing type', () => {
        const todoText = "todo 1";
        cy.get(".new-todo-input")
            .type(todoText)
            .should('have.value', todoText)
    });

    it('save new todo', () => {
        cy.intercept('POST', `${BASE_URL}${saveTodoEndpoint}`, {
            statusCode: 200,
            body: {
                id: 1,
                task: 'Todo 1',
                completed: false
            }
        });

        cy.intercept('GET', `${BASE_URL}${getTodosEndpoint}`, {
            statusCode: 200,
            body: [
                {
                    id: 1,
                    task: 'Todo 1',
                    completed: false
                }
            ]
        });

        const todoText = "todo 1";
        cy.get(".new-todo-input").type(todoText);
        cy.get('.add-btn').click();

        cy.get('.todo-list')
            .find('li')
            .should('have.length', 1)
    });

    it('save new todo with exception', () => {
        cy.intercept('POST', saveTodoEndpoint, {
            statusCode: 500,
        });

        const todoText = "todo 1";
        cy.get(".new-todo-input").type(todoText);
        cy.get('.add-btn').click();

        cy.contains("Todo could not be registered");
    });

    it('show 2 todo when first request', () => {
        cy.intercept('GET', `${BASE_URL}${getTodosEndpoint}`, {
            statusCode: 200,
            body: [
                {
                    id: 1,
                    task: 'Todo 1',
                    completed: false
                },
                {
                    id: 2,
                    task: 'Todo 2',
                    completed: true
                }
            ]
        });

        cy.get('.todo-list')
            .find('li')
            .should('have.length', 2)
    });

    it('is todo finished ?', () => {
        cy.intercept('GET', `${BASE_URL}${getTodosEndpoint}`, {
            statusCode: 200,
            body: [
                {
                    id: 1,
                    task: 'Todo 1',
                    completed: false
                },
                {
                    id: 2,
                    task: 'Todo 2',
                    completed: true
                }
            ]
        });

        cy
            .get('ul>li')
            .eq(1)
            .invoke('css', 'text-decoration')
            .should('equal', 'line-through solid rgb(0, 128, 0)')
    });
});